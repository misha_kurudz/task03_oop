package com.epam.view;



import com.epam.controller.*;
import com.epam.model.*;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Print Shop products");
        menu.put("2", " 2 - Select cleaners");
        menu.put("3", " 3 - Go to the new Shop");
        menu.put("Q", " Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::PressButton1);
        methodsMenu.put("2", this::PressButton2);
        methodsMenu.put("3", this::PressButton3);
    }

    private void PressButton1() {
        System.out.format("%-20s %-20s %-20s %-20s\n", "Type", "Name", "Producer", "Price");
        List lst = controller.getShopProducts();
        for(Object cleaner:lst){
            System.out.println(cleaner);
        }
    }

    private void PressButton2() {
        System.out.println("input type ( '1' - Dishes, '2' - Floor, '3' - Furniture ): ");
        int type = input.nextInt();
        System.out.format("%-20s %-20s %-20s %-20s\n", "Type", "Name", "Producer", "Price");
        List lst = controller.sortSelectedCleaners(type);
        for(Object cleaner:lst){
            System.out.println(cleaner);
        }
    }

    private void PressButton3() {
        controller.goToTheNewShop();
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine();
            try {
                    methodsMenu.get(keyMenu).print();

            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}

