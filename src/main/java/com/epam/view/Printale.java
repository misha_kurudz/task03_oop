package com.epam.view;


@FunctionalInterface
interface Printable {
    void print();
}

